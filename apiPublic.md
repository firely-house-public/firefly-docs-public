
# API Firefly

Essa é a documentação da API Firefly. Aqui você vai encontrar descrição e exemplos para utilizar a API, inteiramente implementada usando GraphQL.

Conteúdo: 

[[_TOC_]]

# GraphQL

O [GraphQL](https://graphql.org) é uma linguagem de query criada pelo Facebook em 2012.

Features:

- Envie uma query GraphQL para a API e receba exatamente o que precisa, nada mais ou a menos. Queries GraphQL sempre retornam resultados previsíveis.
- Uma API GraphQL é auto-documentável. O formato de todas as queries e tipos pode ser retornado pelo servidor [automaticamente](https://graphql.org/learn/introspection/).
- Queries GraphQL podem acessar não somente propriedades do objeto principal mas também acessar (na mesma query) objetos que têm relação com este objeto principal. Enquanto APIs REST requerem o carregamento de múltiplas URLs, as APIs GraphQL podem retornar todos os dados necessários à sua aplicação em uma única query.

# Como utilizar o GraphQL

## Via requisição HTTP

O jeito mais simples de fazer uma query graphql é diretamente por uma requisição HTTP. Exemplo:

```sh
curl \
  -X POST \
  -H "Content-Type: application/json" \
  -H "Authorization: Bearer exampleToken" \
  --data '{ "query": "{ client(id: \"5983706debf3140039d1e8b4\") { id name phone email } }" }' \
  https://example.com/graphql
```

Esta query retorna o seguinte objeto JSON:

```json
{
  "id": "5983706debf3140039d1e8b4",
  "name": "João",
  "phone": "99999999",
  "email": "joao@example.com"
}
```

Nota: exemplo totalmente fictício.

## Utilizando bibliotecas específicas para cada linguagem

Existem várias biliotecas disponíveis para facilitar a integração com GraphQL em sua aplicação. Ver as opções disponíveis no seguinte link:

- https://graphql.org/code/#graphql-clients

Para Node.js, recomendamos o uso de uma das seguintes bibliotecas:

- [Apollo Client](http://apollographql.com/client/)
- [graphql-request](https://github.com/prisma/graphql-request)

## Via GraphQL playground

Para testes da API, o [GraphQL Playground](https://github.com/graphql/graphql-playground) é uma maneira prática de montar e executar diversas *queries* e *mutations*. Além disso, é possível consultar a documentação e formato da api diretamente do servidor.

Acesse o GraphQL playground da Firefly na seguinte URL:

https://api-public.firefly.chat/graphql


# Autenticação com a API

Para autenticar com a API, é necessário, em cada request, enviar um header HTTP contendo o token de API pública do usuário:

| Header        | Valor                             |
|---------------|-----------------------------------|
| Authorization | Bearer <seu_token_de_api_publica> |


Para o GraphQL playground, use o formato JSON, adicionando o seguinte na seção de "HTTP Headers":

```json
{ "Authorization": "Bearer <seu_token_de_api_publica>" }
```

# Endpoints

- Teste: https://api-public.staging.firefly.chat/graphql
- Produção: https://api-public.firefly.chat/graphql


# Queries

A seguir estão descritas as queries que podem ser feitas na API GraphQL.

## externalChatMessageById

Retorna uma mensagem pelo id.

### Parâmetros

| Parâmetro | Tipo | Descrição | Exemplo |
|-|-|-|-|
| id | String! | Id da mensagem (corresponde ao campo `_id` de ExternalChatMessage) | "5f7cf802730b3de0bc0e8e70" |

### Retorno

[ExternalChatMessage](#externalchatmessage)

### Exemplo

```graphql
query {
  externalChatMessageById(
    id: "5f7cf802730b3de0bc0e8e70"
  ) {
    _id
    content
    type
  }
}
```

Este exemplo retorna:

```json
{
  "externalChatMessageById": { "_id": "5f7cf802730b3de0bc0e8e70", "content": "Olá", "type": "TEXT" }
}
```

## externalChatMessagesById

Retorna múltiplas mensagens pelo id. Limite de 1000 ids de mensagens por query.

### Parâmetros

| Parâmetro | Tipo | Descrição | Exemplo |
|-|-|-|-|
| ids | [String!]! | Ids das mensagens (corresponde ao campo `_id` de ExternalChatMessage) | ["5f7cf802730b3de0bc0e8e70", "5f7cf805730b3de0bc0e8e80"] |

### Retorno

[[ExternalChatMessage](#externalchatmessage)!]!

### Exemplo

```graphql
query {
  externalChatMessagesById(
    ids: ["5f7cf802730b3de0bc0e8e70", "5f7cf805730b3de0bc0e8e80"]
  ) {
    _id
    content
    type
  }
}
```

Este exemplo retorna:

```json
{
  "externalChatMessagesById": [
    { "_id": "5f7cf802730b3de0bc0e8e70", "content": "Olá", "type": "TEXT" },
    { "_id": "5f7cf805730b3de0bc0e8e80", "content": "Boa tarde", "type": "TEXT" }
  ]
}
```

# Mutations

A seguir estão descritas as mutations que podem ser feitas na API GraphQL.

## sendExternalChatMessage

Enviar mensagem para canal externo (ex: Whatsapp). A mensagem é inserida em uma fila de envio, e após isso, enviada ao contato. A mutation retorna a mensagem que foi inserida na fila de envio (portanto ainda sem dados específicos do Whatsapp).

Após usar a mutation, para verificar o status de envio da mensagem, se foi de fato enviada, recebida e lida, há trẽs alternativas:

1. Configurar o webhook [externalChatMessageWebhookEvent](#externalchatmessagewebhookevent) para enviar atualizações de status a um endpoint HTTP
2. Usar periodicamente a query [externalChatMessageById](#externalchatmessagebyid) para verificar atualizações de status da mensagem. Nota: há um limite de requisições de API por minuto. Assim, é preferível usar webhooks.
<!-- 3. Usar a GraphQL subscription `externalChatMessageEvent` para receber atualizações diretamente via uma conexão WebSocket -->

### Parâmetros: 

| Parâmetro | Tipo | Descrição | Exemplo |
|-|-|-|-|
| contacts | \[[ExternalChatMessageContact](#externalchatmessagecontact)!\] | Contatos, para mensagem tipo `CONTACT_CARD` | `[{ name: "João", emails: ["joao@gmail.com"] }]` |
| content | String | Conteúdo da mensagem | "Olá" |
| externalChatChannelId | String! | Id do canal externo | "ckfyosq9z00270786c4m6ypxk" |
| file | [Upload](#upload) | (opcional) Arquivo/Imagem para enviar como anexo. Tem precedência sobre o parâmetro `fileUrl`. | Usar graphql-upload para enviar arquivo |
| fileUrl | String | (opcional) Url do arquivo/Imagem para enviar como anexo. | https://example.com/image.jpg |
| location | [ExternalChatMessageLocation](#externalchatmessagelocation) | Localização, para mensagem tipo `LOCATION` | `{ description: "Correios", lat: -25.4481276, lon:  -49.3030389 }` |
| quotedMessageId | String | (opcional) Id da mensagem que está sendo citada, ou respondida. Corresponde ao campo `_id` da mensagem. | "5f7cf802730b3de0bc0e8e70" |
| replyOptions | [String!] | Opções de resposta por botão (apenas suportado em API Whatsapp oficial ou Facebook Messenger) | `["Falar com atendente", "Buscar imóvel"]` |
| sentAt | [DateTime](#datetime) | (opcional) Data em que a mensagem foi enviada | "2020-10-07T03:02:54.170Z" |
| toClientName | String | (opcional) Ao mandar mensagem para um novo cliente, salvar esse nome de contato | "João" |
| toIdInChannel | String! | Para qual número mandar a mensagem. No caso do whatsapp, usar somente números (cod. país + DDD + número), total 12 ou 13 dígitos.  Uma verificação será feita automaticamente para achar o número correto (com ou sem o '9' adicional). | "5541999998888" ou "554199998888" |
| type | [ExternalChatMessageType](#externalchatmessagetype) | Tipo da mensagem | TEXT, IMAGE, DOCUMENT  |

### Retorno

[ExternalChatMessage](#externalchatmessage)


### Exemplo

```graphql
mutation {
  sendExternalChatMessage(
    content: "Olá", 
    sentAt: "2020-10-07T03:02:54.170Z",
    toIdInChannel: "5541999998888",
    type: TEXT
  ) {
    _id
  }
}
```

Este exemplo retorna:

```json
{
  "sendExternalChatMessage": { "_id": "5f7cf805730b3de0bc0e8e93" }
}
```

### Exemplo de mensagem com contato

```graphql
mutation {
  sendExternalChatMessage(
    contacts: [{
      name: "João",
      phones: [{
        phone: "+554122223333",
        type: "cell",
      }]
    }], 
    sentAt: "2020-10-07T03:02:54.170Z",
    toIdInChannel: "5541999998888",
    type: TEXT
  ) {
    _id
    contacts {
      name
      phone {
        phone
        type
      }
    }
  }
}
```

Este exemplo retorna:

```json
{
  "sendExternalChatMessage": { 
    "_id": "5f7cf805730b3de0bc0e8e93",
    "contacts": [{
      "name": "João",
      "phone": {
        "phone": "+554122223333",
        "type": "cell"
      }
    }]
  }
}
```

### Exemplo de mensagem com localização

```graphql
mutation {
  sendExternalChatMessage(
    location: {
      description: "Correios",
      lat: -25.4481276, 
      lon:  -49.3030389
    },
    sentAt: "2020-10-07T03:02:54.170Z",
    toIdInChannel: "5541999998888",
    type: TEXT
  ) {
    _id
    location {
      description
      lat
      lon
    }
  }
}
```

Este exemplo retorna:

```json
{
  "sendExternalChatMessage": { 
    "_id": "5f7cf805730b3de0bc0e8e93",
    "location": {
      "description": "Correios",
      "lat": -25.4481276, 
      "lon":  -49.3030389
    }
  }
}
```

## markExternalChatMessagesForDeletion

Marca as mensagens com o id especificado para serem deletadas. As mensagens são deletadas efetivamente no próximo momento em que isso for possível (quando o celular estiver online).

Importante: A exclusão de mensagens para todos os contatos é apenas suportado em integração whatsapp **não** oficial. Para outras integrações (Whatsapp API oficial ou Facebook Messenger) a mensagem será apenas excluída localmente do servidor Firefly.

Os seguintes campos são setados em cada mensagem no momento da requisição:

- markedForDeletionAt
- markedForDeletionForEveryone
- markedForDeletionUserId

Quando as mensagens são efetivamente deletadas efetivamente, o seguinte ocorre em cada mensagem:

- O campo `type` é mudado para `REVOKED`
- O campo `updatedAt` é atualizado para a data atual
- Os seguintes campos são deletados:
  - contacts
  - content
  - location
  - hasMedia
  - media
  - mediaDownloadPending


### Parâmetros: 

| Parâmetro | Tipo | Descrição | Exemplo |
|-|-|-|-|
| messageIds | [String!]! | Ids das mensagens a serem deletadas | ["5f7cf805730b3de0bc0e8e93"] |
| deleteForEveryone | Boolean | Se `true`, a mensagem será deletada para todos os contatos. Caso contrário, irá ser deletada apenas localmente. | `true` \| `false` |

### Retorno

Boolean - `true` caso houve sucesso em marcar as mensagens para serem deletadas. `false` caso contrário.

### Exemplo

```graphql
mutation {
  markExternalChatMessagesForDeletion(
    messageIds: ["5f7cf805730b3de0bc0e8e93"], 
    deleteForEveryone: true
  )
}
```

Este exemplo retorna:

```json
true
```

## unmarkExternalChatMessagesForDeletion

Desmarca a(s) mensagen(s) com o id especificado para serem deletadas.

Somente terá efeito caso a mensagem ainda não tenha sido deletada do whatsapp. Caso já tenha sido, nenhum ação é tomada com a mensagem.

Os seguintes campos são **removidos** em cada mensagem no momento da requisição:

- markedForDeletionAt
- markedForDeletionForEveryone
- markedForDeletionUserId

### Parâmetros: 

| Parâmetro | Tipo | Descrição | Exemplo |
|-|-|-|-|
| messageIds | [String!]! | Ids das mensagens a serem **desmarcadas** para deletar | ["5f7cf805730b3de0bc0e8e93"] |

### Retorno

Boolean - `true` caso houve sucesso em desmarcar as mensagens para serem deletadas. `false` caso contrário.

### Exemplo

```graphql
mutation {
  unmarkExternalChatMessagesForDeletion(
    messageIds: ["5f7cf805730b3de0bc0e8e93"]
  )
}
```

Este exemplo retorna:

```json
true
```


# Webhooks

Os webhooks podem ser configurados por canal de chat. Cada canal pode ter vários webhooks configurados. Todos os webhooks fazem uma requisição HTTP POST em formato JSON às URLs configuradas.
A resposta pode ser qualquer conteúdo, com um código HTTP 200 para sinalizar que a notificação foi recebida com sucesso.

Atenção: em caso de erro de envio de uma notificação (caso a request retorne um código HTTP diferente de 200), **não há uma lógica de retentativa de envio**.

Abaixo estão descritos os webhooks disponíveis.

## externalChatMessageWebhookEvent

Este webhook manda notificações de eventos de **insert** (nova mensagem) e **update** (mensagem atualizada) nas mensagens do canal. O objeto completo da mensagem é enviado sempre em todos os casos.

### Formato da notificação

Cada notificação deste webhook é um objeto JSON neste formato:
```js
{
  externalChatMessageWebhookEvent: {
    message: ExternalChatMessage,
    operation: 'INSERT' | 'UPDATE',
    verifyToken: String,
    webhookId: String,
  }
}
```

Descrição de cada campo dentro de `externalChatMessageWebhookEvent`:

| Parâmetro | Tipo | Descrição |
|-|-|-|
| message | [ExternalChatMessage](#externalchatmessage)! | Mensagem completa |
| operation | 'INSERT' \| 'UPDATE' | Tipo de operação |
| verifyToken | String! | String secreta (configurada no webhook) para verificação da origem da request |
| webhookId | String! | Id do webhook |

### Exemplo

Abaixo um exemplo de notificação em objeto JSON enviado por este webhook:

```js
{
  externalChatMessageWebhookEvent: {
    message: {
      _id: '5f7cf802730b3de0bc0e8e70',
      _idInChannel: '3EB0A27A38616336B363',
      content: 'Boa tarde!!!',
      createdAt: '2020-10-06T23:04:34.784Z',
      createdAtInChannel: '2020-10-02T19:41:09.000Z',
      date: '2020-10-02T19:41:09.000Z',
      direction: 'RECEIVED',
      externalChatChannelId: 'ckelw24bz001p0786hzi2i1r4',
      from: 'CLIENT',
      fromId: '5f7cf80243dc2c0697299e72',
      fromIdInChannel: '5541999998888@c.us',
      processedAt: '2020-10-06T23:04:34.784Z',
      receivedBy: [
        {
          "date": "2020-07-08T03:28:15.000Z",
          "peer": "CLIENT",
          "peerId": "5f7cf81043dc2c0697299e77"
        }
      ],
      to: 'CHANNEL',
      toId: 'ckelw24bz001p0786hzi2i1r4',
      toIdInChannel: '554133334444@c.us',
      type: 'TEXT',
      updatedAt: '2020-10-06T23:04:34.784Z'
    },
    operation: 'UPDATE',
    verifyToken: '1234',
    webhookId: 'ckg1px74i000c07861te79dyw'
  }
}
```

### Melhorias futuras

- Adicionar retentativa de envio em caso de erros HTTP.


## Ferramentas para teste

Para teste dos webhooks, recomendamos as seguintes ferramentas:
- https://ngrok.com/ - Cria uma url http pública que redireciona para a máquina local.
- https://requestbin.com/ - Cria uma url http pública e salva um histórico online das requisições recebidas.

Nota: essas ferramentas são somente sugestões. Não possuímos nenhuma parceria com as empresas mencionadas.

# Tipos

Aqui estão descritos os tipos usados nas *queries*, *mutations* e *webhooks*.

## ExternalChatMessage

Representa uma mensagem de chat.

| Campo | Tipo | Descrição | Exemplo |
|-|-|-|-|
| _id | String! | Id da mensagem | "5f7cf805730b3de0bc0e8e80" |
| _idInChannel | String | Id da mensagem do canal externo (ex: Whatsapp) | "3EB017E900FABF13E590" |
| contacts | \[[ExternalChatMessageContact](#externalchatmessagecontact)!\] | Contatos, para mensagem tipo `CONTACT_CARD` | `[{ name: "João", emails: ["joao@gmail.com"] }]` |
| content | String | Conteúdo da mensagem. O campo `type` descreve o que representa esse valor | "Olá" |
| createdAt | [DateTime](#datetime) | Quando a mensagem foi criada no banco de dados | "2020-10-06T23:04:37.549Z" |
| createdAtInChannel | [DateTime](#datetime) | Quando a mensagem foi criada no canal externo (ex: Whatsapp) | "2020-08-11T04:24:19.000Z" |
| date | [DateTime](#datetime) | Campo de data principal, usado para filtrar e ordenar as mensagens. Equivale a (createdAtInChannel &#124;&#124; createdAt &#124;&#124; sentAt) | "2020-08-11T04:24:19.000Z" |
| direction | MessageDirection | Direção da mensagem | SENT, RECEIVED |
| errorCode | [ExternalChatMessageErrorCode](#externalchatmessageerrorcode) | Código de erro | CONTACT_ID_VALIDATION_ERROR, MESSAGE_SEND_ERROR  |
| errorMessage | String | Mensagem de erro a ser mostrada para o usuário | "O contato não existe no Whatsapp" |
| externalChatChannelId | String | Id do canal externo | "ckfyosq9z00270786c4m6ypxk" |
| from | [ExternalChatMessagePeer](#externalchatmessagepeer) | Que tipo de contato enviou a mensagem | CHANNEL, CLIENT, USER |
| fromId | String! | Id do contato que enviou a mensagem | "ckfyosq9z00270786c4m6ypxg" |
| fromIdInChannel | String | Id do contato que enviou a mensagem no canal externo (ex: Whatsapp) | "554199998888@c.us" |
| hasError | Boolean | Indica se a mensagem tem um erro | true \| false |
| hasMedia | Boolean | Indica se a mensagem tem anexos (imagem ou arquivo) | true \| false |
| location | [ExternalChatMessageLocation](#externalchatmessagelocation) | Localização, para mensagem tipo `LOCATION` | `{ description: "Correios", lat: -25.4481276, lon:  -49.3030389 }` |
| markedForDeletionAt | [DateTime](#datetime) | A data em que a mensagem foi marcada para ser deletada | "2020-08-11T04:30:00.000Z" |
| markedForDeletionForEveryone | Boolean | Se `true`, a mensagem será deletada para todos os contatos. Caso contrário, irá ser deletada apenas localmente. | "2020-08-11T04:30:00.000Z" |
| markedForDeletionUserId | String | Id do usuário que marcou a mensagem para ser deletada | "akfyosq9z00270786c4m6ypxj" |
| media | [ExternalChatMessageMedia](#externalchatmessagemedia) | Informações do arquivo | [Ver aqui](#externalchatmessagemedia) |
| mediaDownloadPending | Boolean | Indica se o download do anexo está pendente | true \| false |
| processedAt | [DateTime](#datetime) | Quando a mensagem foi processada (enviada ou recebida corretamente) | "2020-10-06T23:04:37.549Z" |
| quotedMessage | ExternalChatMessage | mensagem que está sendo citada, ou respondida. Contém a mensagem completa caso haja resposta, ou null caso contrário | `{ _id: "5f7cf805730b3de0bc0e8e81", content: "oi", type: "TEXT" }` |
| quotedMessageId | String | Id da mensagem que está sendo citada, ou respondida. Corresponde ao campo `_id` da mensagem | "5f7cf805730b3de0bc0e8e81" |
| readBy | ExternalChatMessageReadEvent | Quem leu a mensagem | `[{"date": "2020-08-11T04:24:19.000Z", "peer": CLIENT", "peerId": "5f7cf80543dc2c0697299e73" }]` |
| receivedBy | ExternalChatMessageReadEvent | Quem recebeu a mensagem | `[{"date": "2020-08-11T04:24:19.000Z", "peer": CLIENT", "peerId": "5f7cf80543dc2c0697299e73" }]` |
| replyOptions | [String!] | Opções de resposta por botão (apenas suportado em API Whatsapp oficial ou Facebook Messenger) | `["Falar com atendente", "Buscar imóvel"]` |
| sentAt | [DateTime](#datetime) | (opcional) Quando a mensagem foi enviada | "2020-08-11T03:00:00.000Z" |
| to | [ExternalChatMessagePeer](#externalchatmessagepeer) | Que tipo de contato é o destinatário da mensagem | CHANNEL, CLIENT, USER |
| toId | String! | Id do contato que é destinatário da mensagem | "ckfyosq9z00270786c4m6ykqe" |
| toIdInChannel | String | Id do contato destinatário no canal externo (ex: Whatsapp) | "554133334444@c.us" |
| type | [ExternalChatMessageType](#externalchatmessagetype) | Tipo de mensagem. Descreve o que o campo `content` da mensagem representa. | TEXT, IMAGE, DOCUMENT |
| updatedAt | [DateTime](#datetime) | Data da última atualização da mensagem | "2020-10-06T23:04:37.549Z" |

### Notas

- readBy e receivedBy não são mutuamente exclusivos. Um mesmo cliente pode ter recebido E lido a mensagem.
- readBy e receivedBy podem ser `null` ou `undefined`.

### Exemplo

Abaixo um exemplo completo de mensagem:

```js
{
  _id: '5f7cf802730b3de0bc0e8e70',
  _idInChannel: '3EB0A27A38616336B363',
  content: 'Boa tarde!!!',
  createdAt: '2020-10-06T23:04:34.784Z',
  createdAtInChannel: '2020-10-02T19:41:09.000Z',
  date: '2020-10-02T19:41:09.000Z',
  direction: 'RECEIVED',
  externalChatChannelId: 'ckelw24bz001p0786hzi2i1r4',
  from: 'CLIENT',
  fromId: '5f7cf80243dc2c0697299e72',
  fromIdInChannel: '5541999998888@c.us',
  processedAt: '2020-10-06T23:04:34.784Z',
  readBy: [
    {
      "date": "2020-07-08T03:28:15.000Z",
      "peer": "CLIENT",
      "peerId": "5f7cf81043dc2c0697299e77"
    }
  ],
  receivedBy: [
    {
      "date": "2020-07-08T03:28:10.000Z",
      "peer": "CLIENT",
      "peerId": "5f7cf81043dc2c0697299e77"
    }
  ],
  to: 'CHANNEL',
  toId: 'ckelw24bz001p0786hzi2i1r4',
  toIdInChannel: '554133334444@c.us',
  type: 'TEXT',
  updatedAt: '2020-10-06T23:04:34.784Z'
}
```


## ExternalChatMessageErrorCode
Enum que descreve o código de erro da mensagem.

| Valor | Descrição |
|-|-|
| CONTACT_ID_VALIDATION_ERROR | Erro ao validar o id do contato. Provavelmente o número informado é incorreto |
| MESSAGE_SEND_ERROR | Erro inesperado ao enviar a mensagem |

## ExternalChatMessagePeer

Enum que descreve o tipo de contato que enviou ou recebeu uma mensagem.

| Valor | Descrição |
|-|-|
| CHANNEL | Canal de chat. Quando um cliente manda uma mensagem via Whatsapp, não há como saber o usuário destinatário, portanto a mensagem fica como destino sendo o canal de chat em si |
| CLIENT | Cliente externo |
| USER | Usuário da API Firefly |


## ExternalChatMessageContact

Representa um contato enviado ou recebido.

Os tipos (`type`) de endereço, email e telefone são de acordo com o formato VCard. Ver mais informações em:
https://www.iana.org/assignments/vcard-elements/vcard-elements.xhtml


| Campo | Tipo | Descrição | Exemplo |
|-|-|-|-|
| addresses | \[[ExternalChatMessageContactAddress](#externalchatmessagecontactaddress)!\] | Endereços do cliente |  |
| emails | \[[ExternalChatMessageContactEmail](#externalchatmessagecontactemail)!\] | Emails do cliente |  |
| name | String | Nome do cliente |  |
| phones | \[[ExternalChatMessageContactPhone](#externalchatmessagecontactphone)!\] | Telefones do cliente |  |

## ExternalChatMessageContactAddress

| Campo | Tipo | Descrição | Exemplo |
|-|-|-|-|
| fullAddress | String | Endereço completo | Rua 1, Curitiba, PR, Brasil, 82111-111 |
| city | String | Cidade | Curitiba |
| country | String | País | Brasil |
| countryCode | String | Código do país | BR |
| state | String | Estado | PR |
| street | String | Rua | Rua 1 |
| type | String | Tipo do endereço | "work" \| "home" |

## ExternalChatMessageContactEmail

| Campo | Tipo | Descrição | Exemplo |
|-|-|-|-|
| email | String! | Endereço de email | "joao@gmail.com" |
| type | String | Tipo do email | "work" \| "home" |

## ExternalChatMessageContactPhone

| Campo | Tipo | Descrição | Exemplo |
|-|-|-|-|
| phone | String! | Número de telefone | "554122223333" |
| type | String | Tipo do telefone | "work" \| "home" \| "cell" |

## ExternalChatMessageLocation

| Campo | Tipo | Descrição | Exemplo |
|-|-|-|-|
| description | String | Descrição da localização | "Correios" |
| lat | Float! | Latitude | -25.4481276 |
| lon | Float! | Longitude | -49.3030389 |


## ExternalChatMessageMedia

Representa um anexo (imagem ou arquivo) enviado ou recebido.

| Campo | Tipo | Descrição | Exemplo |
|-|-|-|-|
| fileName | String! | Nome do arquivo | "5f7cf805730b3de0bc0e8e11.jpeg" |
| fileNameOrig | String | Nome original do arquivo | "Imagem 001.jpg" |
| mimetype | String! | Mimetype do arquivo | "image/jpeg" |
| size | Int! | Tamanho em bytes do arquivo | 288477 |
| url | String | Url pública para acesso do arquivo | "https://example.com/5f7cf805730b3de0bc0e8e11.jpeg" |
| urlValidUntil | [DateTime](#datetime) | Até quando a URL é válida | "2020-10-08T23:04:52.327Z" |


## ExternalChatMessageType

Enum que descreve o tipo de mensagem. Descreve o que o campo `content` da mensagem representa.

| Valor | Descrição |
|-|-|
| AUDIO | Mensagem de áudio. **Apenas áudio no formato `audio/ogg; codecs=opus` é suportado** |
| CONTACT_CARD | Contato (formato VCARD) |
| DOCUMENT | Documento/arquivo genérico |
| IMAGE | Imagem |
| LOCATION | Localização |
| REVOKED | Mensagem deletada |
| STICKER | Sticker (**apenas recebimento suportado, não envio**) |
| TEXT | Texto |
| UNKNOWN | Tipo desconhecido |
| VIDEO | Vídeo |
| VOICE | Voz. **Apenas áudio no formato `audio/ogg; codecs=opus` é suportado** |

## DateTime

String que representa uma data em formato ISO date string. Exemplo: "2020-10-08T23:04:52.327Z"

Importante: 

- O horário representado na string com final 'Z' é em UTC. Subtrair 3 horas para chegar ao horário de Brasília.
- É altamente recomendado usar funções de manipulação de datas específicas da sua linguagem, pois o formato ISO string é amplamente suportado.

Exemplo de utilização em Javascript: 

```js
// string -> Date
const date = new Date("2020-10-08T23:04:52.327Z")
// Date -> string
const str = date.toISOString()
```

Ao receber um DateTime no GraphQL, o valor vem como string, e ele precisa ser convertido para `Date` caso necessário.

Ao enviar uma data como variável no GraphQL, tanto um objeto `Date` (do Javascript) quanto uma ISO string podem ser enviados. A conversão de `Date` para string é feita automaticamente.

Exemplo de utilização em PHP:

```php
// string -> DateTime
$date = new DateTime("2020-10-08T23:04:52.327Z");
// DateTime -> string
$str = $date->format("c");
```

## Upload

Tipo usado pela biblioteca [graphql-upload](https://www.npmjs.com/package/graphql-upload) para envio de arquivos diretamente usando GraphQL, no formato de HTTP multipart upload.

Recomendado o uso da biblioteca [apollo-upload-client](https://github.com/jaydenseric/apollo-upload-client) para envio de arquivos do lado do cliente. Ver exemplos de uso na documentação desta biblioteca.

# Tratamento de erros

Os erros GraphQL são tratados em geral de forma diferente dos erros em uma requisição HTTP comum. Os erros podem ser separados em duas categorias:

Erro total: 
- Causas:
  - Ocorreu algum erro de validação da query GraphQL. Algum parâmetro foi passado errado. Por exemplo, String ao invés de Int. Ou algum campo não existente foi solicitado.
  - Ocorreu um erro crítico no servidor.

- Efeitos:
  - Um código HTTP 400 é retornado.
  - É retornado um objeto contendo somente a propriedade `errors` que contém um array de [Error](#objeto-error) 

Erro parcial:
- Causas:
  - Ocorreu algum erro para obter os dados de um campo específico.
  - Ocorreu algum erro de autenticação/autorização.

- Efeitos:
  - Um código HTTP 200 é retornado. 
  - A propriedade `data` é retornada e contém dados parciais. Alguns campos podem ser `null`.
  - A propriedade `errors` é retornada, e contém um array de [Error](#objeto-error).


## Objeto Error

Representa um erro GraphQL. 

Exemplo de query contendo um erro total:

```js
{
  errors: [
    {
      message: 'Cannot query field "externalChatMessageById2" on type "Query". Did you mean "externalChatMessageById"?',
      extensions: {
        code: 'GRAPHQL_VALIDATION_FAILED',
      },
    }
  ]
}
```

Exemplo de query contendo um erro parcial:

```js
{
  errors: [
    {
      message: "O token de autorização é inválido. ",
      locations: [
        {
          "line": 2,
          "column": 3
        }
      ],
      path: [
        "externalChatMessageById"
      ],
      extensions: {
        code: "TokenInvalidError",
      }
    }
  ],
  data: {
    "externalChatMessageById": null
  }
}
```

Descrição dos campos dos erros:

- O campo `message` contém uma mensagem descrevendo o erro
- O campo `extensions.code` contém um código do erro, e pode ser usado por um programa para lidar com cada tipo de erro específico.
- O campo `locations` contém os locais da string da query da que geraram erros.
- O campo `path` (usado somente em erros parciais) contém os caminhos dos campos que sofreram erros.


# Histórico de alterações

## 2020-11-16

- Adicionada seção sobre ferramentas para teste de webhooks
- Adicionada seção sobre tratamento de erros GraphQL
- Removido tipo de mensagem `CONTACT_CARD_MULTI`

## 2020-11-19

- Adicionado campo `fileUrl: String` na mutation `sendExternalChatMessage`

## 2021-04-28

- Adicionado campo `errorCode` em `ExternalChatMessage`
- Adicionado campo `quotedMessage` em `ExternalChatMessage`
- Adicionado campo `quotedMessageId` em `ExternalChatMessage`
- Adicionado campo `quotedMessageId` na mutation `sendExternalChatMessage`

## 2021-06-28

- Adicionada mutation `markExternalChatMessagesForDeletion`
- Adicionada mutation `unmarkExternalChatMessagesForDeletion`
- Adicionados campos em `ExternalChatMessage`:
  - contacts
  - location
  - markedForDeletionAt
  - markedForDeletionForEveryone
  - markedForDeletionUserId
  - replyOptions
- Adicionados campos na mutation `sendExternalChatMessage`:
  - contacts
  - location
  - replyOptions
